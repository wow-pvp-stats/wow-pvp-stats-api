package com.pvpstats.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PvpLeaderboard {

    private String region;
    private int pvpSeasonId;
    private String pvpBracket;
    private String namespace;
}