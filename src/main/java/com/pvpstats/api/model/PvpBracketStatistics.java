package com.pvpstats.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PvpBracketStatistics {

    private String region;
    private String realmSlug;
    private String characterName;
    private String pvpBracket;
    private String namespace;
}