package com.pvpstats.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Realms {

    private String region;
    private String namespace;
}
