package com.pvpstats.api.controllers;

import com.pvpstats.api.service.RequestService;
import com.pvpstats.api.model.Realms;
import com.pvpstats.api.domain.RealmsResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@Tag(name = "Realms", description = "World of Warcraft Realms")
public class RealmsController {

    @GetMapping("/realms/{region}")
    @Operation(summary = "Get a realm from a specific region")
    public RealmsResponse pvpLeaderBoard(@PathVariable String region,
                                         @RequestParam(value = "namespace", defaultValue = "dynamic-eu") String namespace) throws IOException {

        Realms realms = new Realms(region, namespace);

        realms.setRegion(region);
        realms.setNamespace(namespace);

        RequestService requestService = new RequestService();

        return requestService.realmsRequest(region, namespace);
    }
}