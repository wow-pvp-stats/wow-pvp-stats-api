package com.pvpstats.api.controllers;

import com.pvpstats.api.service.RequestService;
import com.pvpstats.api.model.PvpBracketStatistics;
import com.pvpstats.api.domain.PvpBracketStatisticsResponse;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@Tag(name = "PvP Bracket Statistics", description = "World of Warcraft PvP bracket statistics")
public class PvpBracketStatisticsController {

    // Create endpoint to get pvp bracket statistics from a character
    @GetMapping("/{region}/{realmSlug}/{pvpBracket}/{characterName}")
    @Operation(summary = "Get a character bracket statistics")
    public PvpBracketStatisticsResponse pvpBracketStatistics(@PathVariable String region,
                                                             @PathVariable String realmSlug,
                                                             @PathVariable String characterName,
                                                             @PathVariable String pvpBracket,
                                                             @RequestParam(value = "namespace", defaultValue = "profile-eu") String namespace) throws IOException {

        PvpBracketStatistics pvpBracketStatistics = new PvpBracketStatistics(region, realmSlug, characterName, pvpBracket, namespace);

        pvpBracketStatistics.setRegion(region);
        pvpBracketStatistics.setRealmSlug(realmSlug);
        pvpBracketStatistics.setCharacterName(characterName);
        pvpBracketStatistics.setPvpBracket(pvpBracket);
        pvpBracketStatistics.setNamespace(namespace);

        RequestService requestService = new RequestService();

        return requestService.pvpBracketStatisticsRequest(region, realmSlug, characterName, pvpBracket, namespace);
    }
}