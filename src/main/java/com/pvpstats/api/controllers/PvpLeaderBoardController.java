package com.pvpstats.api.controllers;

import com.pvpstats.api.service.RequestService;

import com.pvpstats.api.model.PvpLeaderboard;
import com.pvpstats.api.domain.PvpLeaderboardResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@Tag(name = "PvP Leaderboards", description = "World of Warcraft PvP leaderboards")
public class PvpLeaderBoardController {

    @GetMapping("/{region}/{pvpSeasonId}/{pvpBracket}")
    @Operation(summary = "Get a PvP leaderboard from a specific season and bracket")
    public PvpLeaderboardResponse pvpLeaderBoard(@PathVariable String region,
                                                       @PathVariable int pvpSeasonId,
                                                       @PathVariable String pvpBracket,
                                                       @RequestParam(value = "namespace", defaultValue = "dynamic-eu") String namespace) throws IOException {

        PvpLeaderboard pvpLeaderboard = new PvpLeaderboard(region, pvpSeasonId, pvpBracket, namespace);

        pvpLeaderboard.setRegion(region);
        pvpLeaderboard.setPvpSeasonId(pvpSeasonId);
        pvpLeaderboard.setPvpBracket(pvpBracket);
        pvpLeaderboard.setNamespace(namespace);

        RequestService requestService = new RequestService();

        return requestService.pvpLeaderboardRequest(region, pvpSeasonId, pvpBracket, namespace);
    }
}