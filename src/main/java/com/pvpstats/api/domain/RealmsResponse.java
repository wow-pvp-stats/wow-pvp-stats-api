package com.pvpstats.api.domain;

import com.pvpstats.api.model.Realms;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class RealmsResponse {

    private List<Realms> realms;
}
