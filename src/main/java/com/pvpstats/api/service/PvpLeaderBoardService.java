package com.pvpstats.api.service;

import com.pvpstats.api.domain.PvpLeaderboardResponse;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class PvpLeaderBoardService {

    public PvpLeaderboardResponse GetJsonValuesFromPvpLeaderboard(String json) throws IOException {

        int pvpSeasonId = 0;
        String name = "";
        List<Object> entries = new ArrayList<>();

        PvpLeaderboardResponse pvpLeaderboardResponse = new PvpLeaderboardResponse(pvpSeasonId, name, entries);

        // Init jsonNode to parse the http request json
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(json);

        // Select the specific values we need in the json tree
        pvpSeasonId = jsonNode.path("season").path("id").asInt();
        name = jsonNode.path("name").asText();

        JsonNode jsonArray = jsonNode.get("entries");

        entries = objectMapper.convertValue(jsonArray, ArrayList.class);

        pvpLeaderboardResponse.setPvpSeasonId(pvpSeasonId);
        pvpLeaderboardResponse.setName(name);
        pvpLeaderboardResponse.setEntries(entries);

        return pvpLeaderboardResponse;
    }
}