package com.pvpstats.api.service;

import com.pvpstats.api.domain.PvpBracketStatisticsResponse;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class PvpBracketStatisticsService {

    public PvpBracketStatisticsResponse GetJsonValuesFromPvpBracketStatistics(String json) throws IOException {

        String characterName = "";
        String realmName = "";
        String factionName = "";

        int rating = 0;
        int seasonMatchWon = 0;
        int seasonMatchLost = 0;
        int weeklyMatchWon = 0;
        int weeklyMatchLost = 0;
        int seasonMatchWinrate = 0;
        int weeklyMatchWinrate = 0;

        // Init jsonNode to parse the http request json
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(json);

        PvpBracketStatisticsResponse pvpBracketStatisticsResponse = new PvpBracketStatisticsResponse(characterName, realmName, factionName, rating, seasonMatchWon, seasonMatchLost, seasonMatchWinrate, weeklyMatchWon, weeklyMatchLost, weeklyMatchWinrate);

        // Select the specific values we need in the json tree
        characterName = jsonNode.path("character").path("name").asText();
        realmName = jsonNode.path("character").path("realm").path("name").asText();
        factionName = jsonNode.path("faction").path("name").asText();
        rating = jsonNode.path("rating").asInt();
        seasonMatchWon = jsonNode.path("season_match_statistics").path("won").asInt();
        seasonMatchLost = jsonNode.path("season_match_statistics").path("lost").asInt();
        weeklyMatchWon = jsonNode.path("weekly_match_statistics").path("won").asInt();
        weeklyMatchLost = jsonNode.path("weekly_match_statistics").path("lost").asInt();
        seasonMatchWinrate = CalculateWinrate(seasonMatchWon, seasonMatchLost);
        weeklyMatchWinrate = CalculateWinrate(weeklyMatchWon, weeklyMatchLost);

        pvpBracketStatisticsResponse.setCharacterName(characterName);
        pvpBracketStatisticsResponse.setRealmName(realmName);
        pvpBracketStatisticsResponse.setFactionName(factionName);
        pvpBracketStatisticsResponse.setRating(rating);
        pvpBracketStatisticsResponse.setSeasonMatchWon(seasonMatchWon);
        pvpBracketStatisticsResponse.setSeasonMatchLost(seasonMatchLost);
        pvpBracketStatisticsResponse.setWeeklyMatchWon(weeklyMatchWon);
        pvpBracketStatisticsResponse.setWeeklyMatchLost(weeklyMatchLost);
        pvpBracketStatisticsResponse.setSeasonMatchWinrate(seasonMatchWinrate);
        pvpBracketStatisticsResponse.setWeeklyMatchWinrate(weeklyMatchWinrate);

        return pvpBracketStatisticsResponse;
    }

    private int CalculateWinrate(int win, int lose) {

        // Calculate the winrate from the selected bracket
        return win + lose == 0 ? 0 : win * 100 / (win + lose);
    }
}