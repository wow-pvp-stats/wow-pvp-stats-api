package com.pvpstats.api.service;

import com.pvpstats.api.domain.RealmsResponse;
import com.pvpstats.api.model.Realms;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RealmsService {

    public RealmsResponse GetJsonValuesFromRealms(String json) throws IOException {

        List<Realms> realms = new ArrayList<>();

        RealmsResponse realmsResponse = new RealmsResponse(realms);

        // Init jsonNode to parse the http request json
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(json);

        // Select the specific values we need in the json tree
        JsonNode jsonArray = jsonNode.get("realms");
        realms = objectMapper.convertValue(jsonArray, ArrayList.class);

        realmsResponse.setRealms(realms);

        return realmsResponse;
    }
}
