FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD
ARG SPRING_ACTIVE_PROFILE
MAINTAINER Jasmin
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn clean install
FROM openjdk:11-slim
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/pvpstats-api-*.jar /app/pvpstats-api.jar
ENTRYPOINT ["java", "-jar", "pvpstats-api.jar"]